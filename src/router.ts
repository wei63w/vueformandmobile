import Vue from 'vue'
import Router from 'vue-router'

import Index from './views/Index.vue';
import  Register  from "./views/Register.vue";
import  ValidateSend  from "./views/ValidateSend.vue";


Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'index',
      component: Index
    },
    {
      path: '/index',
      name: 'index',
      component: Index
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/validate',
      name: 'validate',
      component: ValidateSend
    }
  ]
})
