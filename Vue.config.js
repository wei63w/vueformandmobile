module.exports = {
    //配置打包后的环境
    // 查阅 https://cli.vuejs.org/zh/config/#baseurl
    baseUrl: '/',
    outputDir: 'dist',
    lintOnSave: false,//关闭 一堆一堆的warming error提示
    runtimeCompiler: true, //是否使用包含运行时编译器的 Vue 构建版本  ,true 后你就可以在 Vue 组件中使用 template 选项
    // 调整内部的 webpack 配置。
    // 查阅 https://github.com/vuejs/vue-doc-zh-cn/vue-cli/webpack.md
    chainWebpack: () => {},
    configureWebpack: () => {},
    // 配置 webpack-dev-server 行为。
    devServer: {
      open: process.platform === 'darwin',
      host: '0.0.0.0',
      port: 8081,
      https: false,
      hotOnly: false,
      // 查阅 https://github.com/vuejs/vue-doc-zh-cn/vue-cli/cli-service.md#配置代理
      proxy: null, // https://cli.vuejs.org/zh/config/#devserver-proxy
      before: app => {}
    },
  }